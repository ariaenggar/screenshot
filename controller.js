var canvas = document.getElementById("canvas");
if (canvas.getContext){
   var ctx = canvas.getContext("2d");

   ctx.beginPath();

   ctx.moveTo(100,25);
   ctx.quadraticCurveTo(50,25,50,62.5);

   ctx.quadraticCurveTo(50,100,75,100);
   ctx.quadraticCurveTo(75,120,55,125);

   ctx.quadraticCurveTo(85,120,90,100);
   ctx.quadraticCurveTo(150,100,150,62.5);

   ctx.quadraticCurveTo(150,25,100,25);
   ctx.font = "15px Arial";
   ctx.fillText("Hello ARS",65,65);
   ctx.stroke();

   var img = document.getElementById("image");
   ctx.drawImage(img, 5, 100, 50, 50);
}

var sequence = "";
var isDrawing = false;
var mode = "";

document.onkeypress = function(e) {
    e = e || window.event;

    sequence += (String.fromCharCode(e.charCode));
    sequence = sequence.toUpperCase();

    var patternexit = new RegExp("EXIT"),
        patternstart = new RegExp("START");

    if(patternexit.test(sequence) == true && isDrawing){
        isDrawing = false;
        if(mode == "highlight") document.body.removeChild(document.getElementById("highlight"));
        sequence = "";
        alert("Finish drawing");
    }

    if(patternstart.test(sequence) == true && !isDrawing){
        isDrawing = true;
        alert("Start drawing");
        mode = initDraw(document.getElementById("body"));
        //mode = initHighlighter(document.getElementById("body"));
        sequence = "";
    }
};

function initDraw(appElement) {
    var rectangle = null,
        rectangleSize = null,
        mouse = {
            x: 0,
            y: 0,
            startX: 0,
            startY: 0
        };
	
    function setMousePosition(e) {
        var ev = e || window.event; //Moz || IE
        if (ev.pageX) { //Moz
            mouse.x = ev.pageX; //+ window.pageXOffset;
            mouse.y = ev.pageY; //+ window.pageYOffset;
        } else if (ev.clientX) { //IE
            mouse.x = ev.clientX; // + document.body.scrollLeft;
            mouse.y = ev.clientY; // + document.body.scrollTop;
        }
    }

    appElement.onkeyup = function(e){
        if(e.keyCode == 27 && isDrawing){
            if(rectangle){
                rectangle.style.display = "none";
                rectangle = null;
            }
            appElement.style.cursor = "default";
        }
    };

    appElement.onmousemove = function (e) {//updates rectangle size
        if(isDrawing){
            setMousePosition();
            if (rectangle !== null) {
                rectangle.style.left = (mouse.x - mouse.startX < 0) ? mouse.x + "px" : mouse.startX + "px";
                rectangle.style.top = (mouse.y - mouse.startY < 0) ? mouse.y + "px" : mouse.startY + "px";
                rectangle.style.width = Math.abs(mouse.x - mouse.startX) + "px";
                rectangle.style.height = Math.abs(mouse.y - mouse.startY) + "px";
                rectangleSize = {
                    l: parseInt(rectangle.style.left.replace("px","")),
                    t: parseInt(rectangle.style.top.replace("px","")),
                    w: parseInt(rectangle.style.width.replace("px","")),
                    h: parseInt(rectangle.style.height.replace("px",""))
                };
            }
        }
    };

    appElement.onclick = function (e) {
        if (e.button == 0 || e.buttons == 4 || e.which == 1) {
            if (isDrawing) {
                if (rectangle !== null) {
                    document.body.removeChild(rectangle);
                    rectangle = null;
                    appElement.style.cursor = "default";
                    var options = {
                        useCORS : true
                    };

                    html2canvas(document.body,options).then(function (canvas) {
                        var data = canvas.toDataURL("image/png")/*.replace("image/png", "image/octet-stream")*/;

                        //crop and draw in 'canvascrop' canvas
                        var cvc = document.getElementById("canvascrop");
                        var ctxc = cvc.getContext("2d");

                        var imgc = document.createElement("img");
                        imgc.src = data;
                        document.body.appendChild(imgc);

                        ctxc.clearRect(0, 0, cvc.width, cvc.height);
                        cvc.width = rectangleSize.w;
                        cvc.height = rectangleSize.h;
                        ctxc.drawImage(imgc, rectangleSize.l+1, rectangleSize.t+1, rectangleSize.w, rectangleSize.h, 0, 0, rectangleSize.w, rectangleSize.h);
                        document.body.removeChild(imgc);

                        //downloadScreenshot(cvc.toDataURL("image/png"));
                    });
                } else {
                    mouse.startX = mouse.x;
                    mouse.startY = mouse.y;
                    rectangle = document.createElement("div");
                    rectangle.className = "rectangle";
                    rectangle.style.zIndex = 10000;
                    rectangle.style.border = "1px solid #FF0000";
                    rectangle.style.position = "absolute";
                    rectangle.style.left = mouse.x + "px";
                    rectangle.style.top = mouse.y + "px";
                    appElement.appendChild(rectangle);
                    appElement.style.cursor = "crosshair";
                }
            }
        }
    };

    return "draw"
}

function initHighlighter(appElement) {
    var highlight = null;

    getBounds = function (el) {
        var rect = el.getBoundingClientRect();
        var x = parseFloat(el.offsetLeft);
        var y = parseFloat(el.offsetTop);
        var w = parseFloat((rect.right - rect.left))-3;
        var h = parseFloat((rect.bottom - rect.top))-3;
        return {x: x, y: y, w: w, h: h}
    };

    appElement.onmouseover = function (e) {
        if (isDrawing) {
            e = e || window.event;
            var target = e.target || e.srcElement;
            highlight = document.createElement("div");
            highlight.id = "highlight";
            highlight.style.border = "2px solid lightgreen";
            highlight.style.zIndex = 32000;
            highlight.style.position = "absolute";
            highlight.style.pointerEvents = "none";

            var size = getBounds(target);

            highlight.style.left = size.x + "px";
            highlight.style.top = size.y + "px";
            highlight.style.height = size.h + "px";
            highlight.style.width = size.w + "px";
            document.body.appendChild(highlight);
        }
    };

    appElement.onmousemove = function (e) {
        if (isDrawing && highlight) {
            document.body.removeChild(document.getElementById("highlight"));
            document.body.appendChild(highlight);
        }
    };

    appElement.onclick = function (e) {
        if (e.button == 0 || e.buttons == 4 || e.which == 1) {
            if (isDrawing) {
                e = e || window.event;
                var target = e.target || e.srcElement;
                var options = {
                    useCORS : true
                };

                /*html2canvas(document.body, options).then(function (canvas) {
                    var data = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
                    downloadScreenshot(data);
                });*/
            }
        }
    };
    return "highlight";
}

function downloadScreenshot(data, customname) {
    var a = document.createElement("a");
    a.href = data;
    a.id = "downloadlink";
    a.download = customname ? prompt("Please provide name of this screenshot") || "screenshot" + ".png": "screenshot.png";
    document.body.appendChild(a);
    document.getElementById("downloadlink").click();
    document.body.removeChild(a);
}